--
-- Copyright (c) 2019. Adis Nezirovic <anezirovic@haproxy.com>
-- Copyright (c) 2019. Baptiste Assmann <bassmann@haproxy.com>
-- Copyright (c) 2019. Nick Ramirez <nramirez@haproxy.com>
-- Copyright (c) 2019. HAProxy Technologies LLC
-- Copyright (c) 2019. Georges-Etienne Legendre <legege@legege.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, version 2 of the License
--
-- Original Code from https://github.com/haproxytech/haproxy-lua-jwt/blob/73a88c344f2ad5459dd988fdacb40ceb0333645f/lib/jwtverify.lua
--

local json = require 'json'
local base64 = require 'base64'
local openssl = {
  pkey = require 'openssl.pkey',
  digest = require 'openssl.digest',
  x509 = require 'openssl.x509'
}

local _M = {}

local function algorithmIsValid(token)
    if token.header.alg == nil then
        log("No 'alg' provided in JWT header.")
        return false
    elseif token.header.alg ~= 'RS256' then
        log('RS256 supported. Incorrect alg in JWT: ' .. token.header.alg)
        return false
    end

    return true
end

local function signatureIsValid(token, pem)
    local digest = openssl.digest.new('SHA256')
    digest:update(token.header_encoded .. '.' .. token.payload_encoded)
    local vkey = openssl.pkey.new(pem)
    local isVerified = vkey:verify(token.signature, digest)
    return isVerified
end

local function expirationIsValid(token)
    return os.difftime(token.payload.exp, core.now().sec) > 0
end

function _M.encode(playload, expiration_in_sec, pem)
	local header = { typ='JWT', alg='RS256' }
    local header_encoded = base64.urlsafe_encode(json.encode(header))

    playload.exp = core.now().sec + expiration_in_sec
	local payload_encoded = base64.urlsafe_encode(json.encode(playload))

	local digest = openssl.digest.new('SHA256')
    digest:update(header_encoded .. '.' .. payload_encoded)
	local vkey = openssl.pkey.new(pem)
	local signature = vkey:sign(digest)

	local signature_encoded = base64.urlsafe_encode(signature)

    return header_encoded .. '.' .. payload_encoded .. '.' .. signature_encoded
end

function _M.decode(jwtToken, pem)
    local headerFields = core.tokenize(jwtToken, ' .')

    if #headerFields ~= 3 then
        --core.Info('Improperly formated token')
        return nil
    end

    local token = {}
    token.header_encoded = headerFields[1]
    token.header = json.decode(base64.urlsafe_decode(token.header_encoded))
    token.payload_encoded = headerFields[2]
    token.payload = json.decode(base64.urlsafe_decode(token.payload_encoded))
    token.signature_encoded = headerFields[3]
    token.signature = base64.urlsafe_decode(token.signature_encoded)

    --core.Info('JWT Token: ' .. jwtToken)

    -- 2. Verify the signature algorithm is supported (RS256)
    if algorithmIsValid(token) == false then
        --core.Info('Algorithm not valid.')
        return nil
    end

    -- 3. Verify the signature with the certificate
    if signatureIsValid(token, pem) == false then
        --core.Info('Signature not valid.')
        return nil
    end

    -- 4. Verify that the token is not expired
    if expirationIsValid(token) == false then
        --core.Info('Token is expired.')
        return nil
    end

    return token
end

return _M
