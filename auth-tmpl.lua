-- The MIT License (MIT)
--
-- Copyright (c) 2019. Georges-Etienne Legendre <legege@legege.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--

local function interp(s, tab)
	return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end))
end

local _M = {}

local header = [[<html>
<head>
  <title>Auth Portal</title>
</head>
<body>]]

local footer = [[
</body>
</html>]]

local html_login_form = header .. [[
<form method="post">
  <input id="username" name="username" type="username" autocomplete="off" placeholder="Username" autofocus />
  <input id="password" name="password" type="password" autocomplete="off" placeholder="Password" />
  <input id="referer" name="referer" type="hidden" value="${referer}" />
  <button type="submit">Login</button>${message}
</form>]] .. footer

function _M.loginForm(tab)
    return interp(html_login_form, tab)
end

function _M.loginSuccess()
    return header .. [[<p>You're successfully logged in!</p>]] .. footer
end

function _M.unexpectedError()
  return header .. [[<p>Unexpected error!</p>]] .. footer
end

return _M
