-- The MIT License (MIT)
--
-- Copyright (c) 2019. Georges-Etienne Legendre <legege@legege.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--

local config = {
	authServer = nil,
	jwtPem = nil,
	jwtCookieName = nil,
	jwtExpirationInSec = nil
}

local http = require 'socket.http'
local mime = require 'mime'
local cookieParser = require 'cookie'
local tmpl = require 'auth-tmpl'
local jwt = require 'auth-jwt'

local function unescape(s)
	s = string.gsub(s, '+', ' ')
	s = string.gsub(s, '%%(%x%x)', function (h)
		return string.char(tonumber(h, 16))
	end)
	return s
end

local function decodeFormBody(s)
	local variables = {}
	for name, value in string.gmatch(s, '([^&=]+)=([^&=]+)') do
		name = unescape(name)
		value = unescape(value)
		variables[name] = value
	end
	return variables
end

core.register_service('auth-login', 'http', function(applet)
	if applet.method == 'GET' then
		applet:set_status(200)
		applet:add_header('content-type', 'text/html')
		applet:add_header('cache-control', 'no-cache')
		applet:start_response()
		applet:send(tmpl.loginForm({message = '', referer = applet.qs}))
	elseif applet.method == 'POST' then
		if applet.headers['content-type'] == nil or #applet.headers['content-type'] > 1 or applet.headers['content-type'][0] ~= 'application/x-www-form-urlencoded' then
			applet:set_status(400)
			applet:start_response()
			return
		end

		local variables = decodeFormBody(applet:receive())

		local username = variables['username']
		if username == nil then
			username = ''
		end

		local password = variables['password']
		if password == nil then
			password = ''
		end

		local headers = {}
		headers.authorization = 'Basic ' .. mime.b64(username .. ':' .. password)
		local b, c, h = http.request {
			url = config.authServer,
			headers = headers,
			redirect = false
		}

		if b == nil then
			applet:set_status(500)
			applet:add_header('content-type', 'text/html')
			applet:add_header('cache-control', 'no-cache')
			applet:start_response()
			applet:send(tmpl.unexpectedError())
			return
		end

		if c == 200 then
			local payload = {}
			payload.user = username

			local jwtToken = jwt.encode(payload, config.jwtExpirationInSec, config.jwtPem)
			local exp = applet.sc:http_date(applet.f:date(config.jwtExpirationInSec))
			applet:add_header('set-cookie', config.jwtCookieName .. '=' .. jwtToken .. '; Expires=' .. exp .. '; HttpOnly; Secure')

			if variables['referer'] == nil then
				applet:set_status(200)
				applet:add_header('content-type', 'text/html')
				applet:add_header('cache-control', 'no-cache')
				applet:start_response()
				applet:send(tmpl.loginSuccess())
			else
				applet:set_status(302)
				applet:add_header('location', variables['referer'])
				applet:start_response()
			end
		else
			applet:set_status(403)
			applet:add_header('content-type', 'text/html')
			applet:add_header('cache-control', 'no-cache')
			applet:start_response()
			applet:send(tmpl.loginForm({message = '<p>Invalid username/password!</p>', referer = variables['referer']}))
		end
	else
		applet:set_status(405)
		applet:start_response()
	end
end)

core.register_action('auth-verify', {'http-req'}, function(txn)
	local cookies = {}
	local jwtToken = nil

	txn:set_var('txn.auth_authenticated', false)

	-- 0. Get cookie
	for header, values in pairs(txn.http:req_get_headers()) do
		for i, v in pairs(values) do
			if header == 'cookie' then
				cookies = cookieParser.get_cookie_table(v)
			end
		end
	end

	for k,v in pairs(cookies) do
		if k == config.jwtCookieName then
			jwtToken = v
		end
	end

	if jwtToken == nil then
		return
	end

	-- 1. Decode and parse the JWT
    local token = jwt.decode(jwtToken, config.jwtPem)

    if token == nil then
		return
    end

    -- 2. Set authenticated variable
	txn:set_var('txn.auth_user', token.payload.user)
    txn:set_var('txn.auth_authenticated', true)
end)

local function readAll(file)
    local f = assert(io.open(file, 'rb'))
    local content = f:read('*all')
    f:close()
    return content
end

core.register_init(function()
	config.authServer = os.getenv('AUTH_SERVER')
	config.jwtPem = readAll(os.getenv('AUTH_JWT_PEM_PATH'))
	config.jwtCookieName = os.getenv('AUTH_JWT_COOKIE_NAME')
	config.jwtExpirationInSec = os.getenv('AUTH_JWT_EXPIRATION') * 1
end)
