# HAProxy Auth Portal in Lua

## Dependencies

* [luasocket](https://github.com/diegonehab/luasocket)
* [luaossl](https://github.com/wahern/luaossl)
* [lua-json](https://github.com/rxi/json.lua)
* base64 (included in this repository, modified version from [JWT library for HAProxy](https://github.com/haproxytech/haproxy-lua-jwt/blob/73a88c344f2ad5459dd988fdacb40ceb0333645f/lib/base64.lua))
* cookie (included in this repository, modified version from [Lua Resty Cookie](https://github.com/cloudflare/lua-resty-cookie))

## HAProxy Configuration

```
global
    ...
    setenv AUTH_JWT_PEM_PATH /etc/haproxy/jwtcookie.pem
    setenv AUTH_SERVER http://127.0.0.1:8080
    setenv AUTH_JWT_COOKIE_NAME JWT
    setenv AUTH_JWT_EXPIRATION 3600

frontend https
    mode http
    bind :80
    # SSL is required because the cookie returned is marked as `Secure`
    bind :443 ssl ...

    acl login path_beg /login
    http-request redirect scheme https code 302 if !{ ssl_fc } login
    http-request use-service lua.auth-login if login
    http-request lua.auth-verify if { req.cook(JWT) -m found }

    use_backend protected_backend if { path_beg /private }

    default_backend default

backend protected_backend
    http-request redirect scheme https code 302 if !{ ssl_fc }
    acl is_auth_authenticated location /login?%[url] if !is_auth_authenticated
    server protected 127.0.0.1:8000
```

## Key Pair Generation

```
openssl genpkey -algorithm RSA -out private.pem -pkeyopt rsa_keygen_bits:2048
openssl rsa -pubout -in private.pem -out public.pem
cat private.pem public.pem > jwtcookie.pem
```

## References 

* [HAProxy Auth Request](https://github.com/TimWolla/haproxy-auth-request/)
* [App in HAProxy](https://github.com/TimWolla/h-app-roxy)
* [JSON Web Token (JWT) library for HAProxy](https://github.com/haproxytech/haproxy-lua-jwt)
* [HAProxy Authentication with Vault](https://github.com/csawyerYumaed/hapvault)